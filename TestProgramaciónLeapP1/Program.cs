﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgramaciónLeapP1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Program p = new Program();
            for (int i = 0; i <= 20; i++)
            {
                Console.WriteLine("n = "+i+ " es " + p.xyz(i));
            }
            //Console.WriteLine("n = " + 2 + " es " + p.xyz(2));
            Console.ReadKey();
            
        }

        public bool xyz(int n)
        {
            //Console.WriteLine("n antes de raiz cuadrada es = " + n);
            double i = Math.Sqrt(n);
            //Console.WriteLine("i es "+i);
            double j = Math.Ceiling(i);
            //Console.WriteLine("j es " + j); 
            int k = 2;
            int x = k;
            while(x<=j)
            {
                //Console.WriteLine("x es "+x+" j es "+j);
                //Console.WriteLine("(n%x) es " + (n % x));
                //Console.WriteLine("Convert.ToBoolean(n%x) es " + Convert.ToBoolean(n % x));
                //Console.WriteLine("!Convert.ToBoolean(n%x) es " + !(Convert.ToBoolean(n % x)));
                if (!(Convert.ToBoolean(n % x))) return false;
                else x++;
            }

            return true;
        }
       
    }

    
    
}
